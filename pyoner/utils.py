#!/usr/bin/env python3

__all__ = ["discretize_the_data_set", "get_attr_means", "predict", "accuracy", "describe_accuracy_by_step"]
__author__ = 'Damian Giebas'
__copyright___ = "Copyright (c) 2018 Damian Giebas"
__email__ = 'damian.giebas@gmail.com'
__name__ = "utils"
__version__ = '1.0'

import numpy as np


def discretize_the_data_set(data_set, attribute_means):
    return np.array(data_set >= attribute_means, dtype="int")  # Compute the arithmetic mean along the specified axis.


def get_attr_means(data_set, axis):
    return data_set.mean(axis=axis)


def predict(data_test, model):
    variable = model.feature
    predictor = model.predictors
    return np.array([predictor[sample[variable]] for sample in data_test])


def accuracy(target_predicted, target_test):
    return np.mean(target_predicted == target_test) * 100


def describe_accuracy_by_step(predicted_data, real_data):
    iter = 0
    for prediction, real_value in zip(predicted_data, real_data):
        print("{}: Preadicted value: {}\tReal value: {} Accuracy: {:.1f}".format(iter + 1, prediction, real_value,
                                                                                 accuracy(predicted_data[:iter + 1],
                                                                                          real_data[:iter + 1])))
        iter += 1
