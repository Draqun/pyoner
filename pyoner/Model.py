#!/usr/bin/env python3

__all__ = ["Model"]
__author__ = 'Damian Giebas'
__copyright___ = "Copyright (c) 2018 Damian Giebas"
__email__ = 'damian.giebas@gmail.com'
__name__ = "Model"
__version__ = '1.0'

from collections import defaultdict
from operator import itemgetter


class Model:
    """Representation of data model using OneR algorithm for training"""

    def __init__(self, training_data, verification_data):
        """
        Ctor
        :param training_data: Set of data used to train model
        :param verification_data: Set od data used to verify model
        """
        self._training_data = training_data
        self._verification_data = verification_data
        self._feature = None
        self._predictors = None
        self._minor_error = None

    def __train_feature_value(self, feature_index, value):
        """
        Method which calculate error rate and search most frequent class using feature_index and value.
        Function this use real data and count how much feature values from  _training_data are equal predicted value.
        :param feature_index: Index of specified feature used to get proper data from verification data
        :param value: Value of specified feature from _training_data
        :return: Tuple with most frequent class and error rate
        """

        class_counts = defaultdict(int)

        for sample, verification_sample in zip(self._training_data, self._verification_data):
            """Iterate over data and verification data"""
            if sample[feature_index] == value:  # If sample from _training_data is equal predicted value
                class_counts[verification_sample] += 1  # Then we increase counter for verification value

        # Sort class counts to find most frequent class
        sorted_class_counts = sorted(class_counts.items(), key=itemgetter(1), reverse=True)
        most_frequent_class = sorted_class_counts[0][0]

        # Computing the error rate for most frequent class
        incorrect_predictions = [class_count for class_value, class_count in class_counts.items()
                                 if class_value != most_frequent_class]
        error = sum(incorrect_predictions)
        return most_frequent_class, error

    def __train(self, feature_index):
        """
        Function creating predictors and error rate for specified feature using feature index for training data
        :param feature_index: Index of specified feature
        :return: tuple with predictors values and error rate
        """
        values = set(self._training_data[:, feature_index])

        predictors = defaultdict(int)
        errors = 0
        for current_value in values:
            most_frequent_class, error = self.__train_feature_value(feature_index, current_value)
            predictors[current_value] = most_frequent_class
            errors += error
        return predictors, errors

    def train(self):
        """
        Function searching best model.
        Best model is chosen from set of predictions created by comparing predicted value with real value for all
        feature. Best prediction is chosen by search feature prediction with the lowest error rate.
        """
        for feature_index in range(self._training_data.shape[1]):
            predictors, total_error = self.__train(feature_index)
            if self._minor_error is None:
                self._minor_error = total_error
                self._feature = feature_index
                self._predictors = predictors
            elif self._minor_error > total_error:
                self._minor_error = total_error
                self._feature = feature_index
                self._predictors = predictors

    @property
    def feature(self):
        if self._feature is None:
            self.train()
        return self._feature

    @property
    def predictors(self):
        if self._predictors is None:
            self.train()
        return self._predictors

    @property
    def minor_error(self):
        if self._minor_error is None:
            self.train()
        return self._minor_error
