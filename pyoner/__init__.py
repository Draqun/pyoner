#!/usr/bin/env python3

__all__ = ["Model", "discretize_the_data_set", "get_attr_means", "predict", "accuracy", "describe_accuracy_by_step"]
__author__ = 'Damian Giebas'
__copyright___ = "Copyright (c) 2018 Damian Giebas"
__email__ = 'damian.giebas@gmail.com'
__name__ = "pyoner"
__version__ = '1.0'

from .Model import Model
from .utils import *
