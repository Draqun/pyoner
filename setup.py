import setuptools

with open("README.md") as file_handler:
    long_description = file_handler.read()

setuptools.setup(
    name = "PyOneR",
    version = "1.0.3",
    author = "Damian 'Draqun' Giebas",
    author_email = "damian.giebas@gmail.com",
    description = "Implementation of OneR algorithm.",
    license = "Dual license",
    long_description = long_description,
    long_description_content_type = "text/markdown",
    url = "https://gitlab.com/Draqun/pyoner",
    packages = setuptools.find_packages(),
    platforms = ["Windows", "Linux", "Solaris", "Mac OS-X", "Unix"],
    classifiers = [
        "Programming Language :: Python :: 3",
        "License :: Other/Proprietary License",
        "Operating System :: Microsoft :: Windows",
        "Operating System :: POSIX",
        "Operating System :: Unix",
        "Operating System :: MacOS",
        "Topic :: Scientific/Engineering :: Artificial Intelligence",
        "Topic :: Software Development :: Libraries :: Python Modules"
    ]
)
