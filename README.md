# PyOneR
PyOneR is implementation of OneR algorithm in Python.



## License
PyOneR is released under dual license. For open source project source code is distributed under GNU/GPLv3. For commercial use please contact with author using mail: damian.giebas@gmail.com

For more read [License](License)


## Changelog
```
v1.0.3
    - Default predictor class is changed from dict to defaultdict
```

```
v1.0.2
    - Fix for function name from utils module
```